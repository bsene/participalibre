<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Project extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * Generate a random token to project
     *
     * @return void
     */
    public function createAdminToken(): void
    {
        $this->fill([
            'admin_token' => Str::random(),
        ])->save();
    }

    /**
     * Return link with token to edit project
     *
     * @return string
     */
    public function getPathAdminAttribute(): string
    {
        return route('projects.edit', [$this, $this->admin_token]);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
