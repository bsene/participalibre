<!DOCTYPE html>
<html lang="fr" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title') ⋅ Contribulle</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="shortcut icon" href="/images/logo.png" type="image/x-icon">
</head>

<body>
    <header class="big-header">
        <div class="columns">
            <div class="column columns flex-items-center">
                <img class="margin-small-right" src="/images/logo.svg" alt="Logo de contribulle" height="75px"
                    width="75px">
                <div class="home-link">
                    <a href="/">
                        <h1>
                            Contribulle
                        </h1>
                        <sub>
                            La contribution pour tout le monde !
                        </sub>
                    </a>
                </div>
            </div>
            <div class="column text-right">
                @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                    <a href="{{ url('/home') }}">Home</a>
                    @else
                    <a href="{{ route('login') }}">Login</a>
                    @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                    @endif
                    @endauth
                </div>
                @endif
                <nav class="links text-normal">
                    <a href="/about">
                        À propos
                    </a>
                </nav>
            </div>
        </div>

    </header>
    <main>
        <header>
            <div class="inverse">
                <p class="text-center padding-small margin-none">
                    Contribulle est tout frais ! L'équipe attend vivement vos retours sur les réseaux <a
                        href="https://mastodon.tedomum.net/@contribulle" target="_blank"
                        rel="noopener noreferrer">Mastodon</a>
                    / <a href="https://twitter.com/contribulle" target="_blank" rel="noopener noreferrer">Twitter</a>
                    ou par mail <a href="mailto:bonjour[arobase]contribulle.org">bonjour[arobase]contribulle.org</a>,
                    bonne navigation !
                </p>
            </div>
            @yield('content-header')
        </header>
        <section id="content">
            @yield('content')
        </section>
    </main>
    <footer class="big-footer">
        <div>
            Pour dire coucou ou pour faire un retour : <a
                href="mailto:bonjour[arobase]contribulle.org">bonjour[arobase]contribulle.org</a>
        </div>
        <div>
            Contribulle 2021 — Ce site est placé sous licence
            <a href="https://www.gnu.org/licenses/agpl-3.0.txt" target="_blank">GNU AGPLv3+</a>
            — <a href="https://framagit.org/participalibre/participalibre" target="_blank">Code source</a>
        </div>
    </footer>
    <script defer="true" src="/js/app.js"></script>
    @yield('script')
</body>

</html>