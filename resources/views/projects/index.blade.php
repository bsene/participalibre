@extends('layouts.app')

@section('title', "Les projets qui ont besoin d’aide")

@section('content-header')
<div class="padding-medium background-accent overflow-hidden">
    <h1>C’est votre première contribution et vous êtes perdu⋅e⋅s ?
        <br>
        On vous a sélectionné quelques projets
    </h1>
    <div id="slider">
        @foreach ($contributions as $contribution)
        <a href="{{ $contribution['link'] }}" class="slider-content text-decoration-none" target="_blank">
            <div class="card-header">
                <h2 class="text-normal">{{ $contribution['name'] }}</h2>
            </div>
            <div class="card-body">
                <p>
                    {{ $contribution['description'] }}
                </p>
            </div>
            <div class="inverse card-footer text-right">
                <i class="arrow-right"></i>
                <span>Voir le site</span>
            </div>
        </a>
        @endforeach
    </div>
</div>
@endsection

@section('content')
<section>
    <h1>Les projets qui ont besoin d’un coup de main</h1>
    <div class="child-margin-vertical-medium">
        @foreach ($projects as $project)

        <article class="card card-radius">
            <div class="card-header columns flex-wrap">
                <span class="category">{{ $project->tags()->first()->category->name }}</span>
                @foreach ($project->tags as $tag)
                <span class="tag">{{ $tag->name }}</span>
                @endforeach
            </div>
            <hr>
            <a class="card-body text-decoration-none" target="_blank" href="/projects/{{ $project->id }}" style="display: block">
                <h2 class="text-normal">{{ $project->name }}</h2>
                <p>
                    {{ $project->description }}
                </p>
            </a>
            <a href="/projects/{{ $project->id }}" style="display: block" class="inverse card-footer text-right">
                <i class="arrow-right"></i>
                <span>Voir l’annonce</span>
            </a>
        </article>
        @endforeach
    </div>
</section>
@endsection

@section('script')
<script defer src="/js/slider.js"></script>
@endsection
