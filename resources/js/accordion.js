class Accordions {
  /**
   * Represents an accordion elements group.
   * @params {string} className - The className of an accordion.
   */
  constructor(className) {
    this.accordions = [];

    document.querySelectorAll("." + className).forEach((el) => {
      const toggleElement = el.querySelector(".toggle-element");
      toggleElement.addEventListener("click", (ev) => this.clicked(ev.target));

      this.accordions.push({
        el: el,
        elInitHeight: el.clientHeight,
        toggleElement,
        button: el.querySelector(".chevron-up"),
        contentInitHeight: el.querySelector(".content").clientHeight,
        expand: !!toggleElement.className.search("chevron-up"),
      });
    });
    this.hideAllContent();
  }

  clicked(button) {
    /**
     * Method called when a button is clicking.
     * Toggle content.
     * @param button - The element clicked.
     */
    const el = this.accordions.find(
      (accordion) =>
        accordion.toggleElement === button ||
        accordion.toggleElement === button.parentElement ||
        accordion.toggleElement === button.parentElement.parentElement
    );
    if (el.expand) {
      this.hideContent(el);
    } else {
      this.displayContent(el);
    }
  }

  hideContent(el) {
    /**
     * Changes the height of the element and the class of button for change icon.
     * @param el - Object containing el, content and toggleElement
     */
    // el.content.className += ' hidden';
    el.el.style.height = el.elInitHeight - el.contentInitHeight + "px";
    el.button.className = el.button.className.replace(
      "chevron-up",
      "chevron-down"
    );
    el.expand = false;
  }

  displayContent(el) {
    /**
     * Restores the original height and changes the class of button for change icon.
     * @param el - Object containing el, content and toggleElement
     */
    this.hideAllContent();
    el.expand = true;
    el.el.style.height = el.elInitHeight + "px";
    el.button.className = el.button.className.replace(
      "chevron-down",
      "chevron-up"
    );
  }

  hideAllContent() {
    /**
     * Calls the hideContent method for all elements.
     */
    this.accordions.forEach((accordion) => {
      this.hideContent(accordion);
    });
  }
}

const accordion = new Accordions("accordion");
